﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioScheduled : MonoBehaviour {

	public float startTime;
	public float endTIme;
	public AudioSource _audioSource;

	float _start;// = _audioSource.clip.length * startTime;//.value;
	float _end;// = 

	public bool isplaying;

	public Animation _animation;

	string[] ani_names;
	public string selectClipName;
	// Use this for initialization
	void Start () {
		_audioSource = GetComponent<AudioSource> ();
		_animation = GetComponent<Animation> ();

		_start = _audioSource.clip.length * startTime;//.value;
		_end = _audioSource.clip.length * endTIme;//.value;

		int size = _animation.GetClipCount();
		ani_names = new string[size];
		int counter = 0;
		foreach (AnimationState states in _animation) {
			ani_names[counter++] = states.name;
		}


		PlayScheduledAudioAndAnimation ();
	}
	
	// Update is called once per frame
	void Update () {
		if (_animation [selectClipName].normalizedTime == 0 && !_audioSource.isPlaying) {
			PlayScheduledAudioAndAnimation ();
		}

		isplaying = _audioSource.isPlaying;

//		if () 
//		{
//			
//		}
	}

	void PlayScheduledAudioAndAnimation()
	{
		if (ani_names.Length != 1) {
			selectClipName = ani_names [Random.Range (0, ani_names.Length)];
		} else
		{
			selectClipName = ani_names [0];	
		}

		_animation.Play (selectClipName);
		_audioSource.time = _start;
		_audioSource.Play();
		_audioSource.SetScheduledEndTime(AudioSettings.dspTime+(_end-_start));
	}

	public void StopScheduledAudioAndAnimation()
	{
		_audioSource.Stop();
		_animation.Stop (selectClipName);
	}


	void OnDisable() {
//		Debug.Log (gameObject.name + "OnDisable");
		StopScheduledAudioAndAnimation ();
	}

}
