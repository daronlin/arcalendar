﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class Console : MonoBehaviour
{
	void Awake()
	{
		DontDestroyOnLoad (gameObject);
	}

	struct Log
	{
		//儲存Log用class
		public string dateTime;        //發生的時間
		public string message;        //Log訊息
		public string stackTrace;    //function stack
		public LogType type;        //Log類型
	}

	//存放所有LOG
	List<Log> logs = new List<Log>();
	Vector2 scrollPosition;
	//更新到最下方的LOG位置
	Vector2 scrollEndPosition = new Vector2(0, 999999999);
	bool show;
	bool collapse;
	bool FinalLog;

	bool showNormalLog = true;
	bool showWarningLog = true;
	bool showErrorLog = true;

	//LOG類型 顏色 區分 
	static readonly Dictionary<LogType, Color> logTypeColors = new Dictionary<LogType, Color>()
	{
		{ LogType.Assert, Color.white },
		{ LogType.Error, Color.red },
		{ LogType.Exception, Color.red },
		{ LogType.Log, Color.white },
		{ LogType.Warning, Color.yellow },
	};

	const int margin = 20;

	Rect windowRect = new Rect(margin, margin, Screen.width - (margin * 2), Screen.height - (margin * 2));
	Rect titleBarRect = new Rect(0, 0, Screen.width - (margin * 2), 50);
	GUIContent clearLabel = new GUIContent("Clear", "Clear the contents of the console.");
	GUIContent collapseLabel = new GUIContent("Collapse", "Hide repeated messages.");
	GUIContent newLogLabel = new GUIContent("FinalLog", "FinalLog messages.");

	GUIContent showNormalLogLabel = new GUIContent("NormalLog", "LogType : NormalLog");
	GUIContent showWarningLogLabel = new GUIContent("WarningLog", "LogType : WarningLog");
	GUIContent showErrorLogLabel = new GUIContent("ErrorLog", "LogType : ErrorLog");

	void OnEnable ()
	{
		Application.RegisterLogCallback(HandleLog);
	}

	void OnDisable ()
	{
		Application.RegisterLogCallback(null);
	}

	void OnGUI ()
	{
		//按鈕式 開關
		if (GUI.Button(new Rect(10, 10, 100, 50), "ShowLog")){
			show = !show;
		}
		if (!show) {
			return;
		}

		windowRect = GUILayout.Window(123456, windowRect, ConsoleWindow, "Console");
	}

	void ConsoleWindow (int windowID)
	{
		//開始 ScrollView 範圍 ---------------------------------------------------------------------------------------------------------------------------
		scrollPosition = GUILayout.BeginScrollView(scrollPosition);
		//將訊息一筆一筆放入
		for (int i = 0; i < logs.Count; i++) {
			var log = logs[i];

			//一樣的訊息跳過
			if (collapse) {
				var messageSameAsPrevious = i > 0 && log.message == logs[i - 1].message;

				if (messageSameAsPrevious) {
					continue;
				}
			}
			if (!showNormalLog)
			{//不顯示 一般LOG
				if(log.type == LogType.Log || log.type == LogType.Assert)
					continue;
			}
			if (!showWarningLog) 
			{//不顯示 警告LOG
				if(log.type == LogType.Warning)
					continue;
			}
			if (!showErrorLog) 
			{//不顯示 錯誤LOG
				if(log.type == LogType.Error || log.type == LogType.Exception)
					continue;
			}

			GUI.contentColor = logTypeColors[log.type];
			GUILayout.TextArea(string.Format("{0}  >>>>>>>>>>>>>>>{1}<<<<<<<<<<<<<<<\n{2}\n",log.dateTime,log.message,log.stackTrace));
		}
		GUILayout.EndScrollView();
		//結束ScrollView-----------------------------------------------------------------------------------------------------------------------------------

		GUI.contentColor = Color.white;

		//底排功能按鈕 Horizontal 橫向排列 --------------------------------------------------------------------------------
		GUILayout.BeginHorizontal();

		if (GUILayout.Button(clearLabel)) {
			logs.Clear();
		}

		if (GUILayout.Button(newLogLabel)) {
			scrollPosition = scrollEndPosition;
		}

		collapse = GUILayout.Toggle(collapse, collapseLabel, GUILayout.ExpandWidth(false));
		showNormalLog = GUILayout.Toggle(showNormalLog, showNormalLogLabel, GUILayout.ExpandWidth(false));
		showWarningLog = GUILayout.Toggle(showWarningLog, showWarningLogLabel, GUILayout.ExpandWidth(false));
		showErrorLog = GUILayout.Toggle(showErrorLog, showErrorLogLabel, GUILayout.ExpandWidth(false));
		GUILayout.EndHorizontal();
		//結束底排--------------------------------------------------------------------------------------------------------

		//可以拖曳
		GUI.DragWindow(titleBarRect);
	}

	//UNITY LOG 從 Application.RegisterLogCallback(HandleLog) 取得
	void HandleLog (string message, string stackTrace, LogType type)
	{
		DateTime nowDate;
		nowDate = DateTime.Now;
		logs.Add(new Log() {
			dateTime = string.Format("[{0}]", nowDate.ToString()),
			message = message,
			stackTrace = stackTrace,
			type = type,
		});
	}
}