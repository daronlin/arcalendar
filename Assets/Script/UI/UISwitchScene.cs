﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UISwitchScene : MonoBehaviour {

	public Button BtnAR;
	public Button BtnCalendar;

	public Sprite BtnARNormal;
	public Sprite BtnARSelect;

	public Sprite BtnCalendarNormal;
	public Sprite BtnCalendarSelect;

	public Button BtnARIntroduction;

	public string[] SceneName;

	public GameObject loadOBJ;

	public Button BtnInfo;
	public GameObject _IntroInfo;
	public ScrollRect _IntroScrollview;

	// Use this for initialization
	void Start () {
		BtnInfo.onClick.AddListener (OnClickIntroInfo);
		BtnAR.onClick.AddListener(OnClickAR);
		BtnCalendar.onClick.AddListener(OnClickCalendar);
		BtnARIntroduction.onClick.AddListener(HideIntroduction);

//		Debug.Log(SceneManager.GetActiveScene().name);
	}

	void Update()
	{
		//AR
		if(SceneManager.GetActiveScene().name == SceneName[0])
		{
			BtnAR.image.sprite = BtnARSelect;
			BtnCalendar.image.sprite = BtnCalendarNormal;
			if(PlayerPrefs.GetInt("FirstOpenAR")==0)
			{
				BtnARIntroduction.gameObject.SetActive(true);
			}
		}
		else if(SceneManager.GetActiveScene().name == SceneName[1])//日歷
		{
			BtnAR.image.sprite = BtnARNormal;
			BtnCalendar.image.sprite = BtnCalendarSelect;
		}
	}

	void OnClickAR()
	{
		if(SceneManager.GetActiveScene().name == SceneName[0])
			return;
		else
		{
			StartCoroutine (LoadAnimation (0));
//			loadOBJ.SetActive(true);
//			SceneManager.LoadScene(SceneName[0],LoadSceneMode.Single);
//			SceneManager.LoadScene("UI",LoadSceneMode.Additive);
		}
	}

	void OnClickCalendar()
	{
		if(SceneManager.GetActiveScene().name == SceneName[1])
			return;
		else
		{
			StartCoroutine (LoadAnimation (1));
//			loadOBJ.SetActive(true);
//			SceneManager.LoadScene(SceneName[1],LoadSceneMode.Single);
//			SceneManager.LoadScene("UI",LoadSceneMode.Additive);
		}
	}

	IEnumerator LoadAnimation(int sceneIdx)
	{

		loadOBJ.SetActive(true);
		yield return new WaitForSeconds(1.0f+Time.deltaTime);
		SceneManager.LoadScene(SceneName[sceneIdx],LoadSceneMode.Single);
		SceneManager.LoadScene("UI",LoadSceneMode.Additive);
	}

	void HideIntroduction()
	{
		PlayerPrefs.SetInt("FirstOpenAR",1);
		BtnARIntroduction.gameObject.SetActive(false);
	}
		
	void OnClickIntroInfo()
	{
		_IntroScrollview.normalizedPosition = new Vector2 (0, 1);
		_IntroInfo.SetActive (!_IntroInfo.activeSelf);
//		_IntroScrollview.
//		if (_IntroInfo.activeSelf) {
//			_IntroInfo.SetActive (false);
//		} else {
//			_IntroInfo.SetActive (true);
//		}
	}
}

