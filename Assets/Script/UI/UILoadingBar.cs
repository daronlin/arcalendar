﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UILoadingBar : MonoBehaviour {

	public Image 	_loadBar;
	public Text 	_percentText;
	public float	_percent;
	public Text		_tipText;

	public float	_loadTimeSpeed = 0.13f;

	public string[]	TWTipList;
	public string[] ENTipList;

	public delegate void onLoadDoneEvent ();
	public onLoadDoneEvent OnLoadDoneEvent;
	public bool isLoadDone = false;

	public float Percent
	{
		get{
			return _percent;
		}
		set{
			_percent = value;
			_tipText.text = TWTipList [Random.Range (0, TWTipList.Length)];
		}
	}

	// Use this for initialization
	void Start () {
		_percent = 0.0f;
		_tipText.text = "";
		_percentText.text = "";
	}

	// Update is called once per frame
	void Update () {
		if (_percent >= _loadBar.fillAmount) {
			_loadBar.fillAmount += Time.deltaTime * _loadTimeSpeed;// Mathf.Lerp (0.0f, 1.0f, _loadBar.fillAmount+Time.deltaTime);
			_percentText.text = Mathf.FloorToInt (_loadBar.fillAmount * 100.0f).ToString () + "%";
		}
		if(_loadBar.fillAmount == 1 && !isLoadDone)
		{
			isLoadDone = true;
			OnLoadDoneEvent();
		}
	}
}
