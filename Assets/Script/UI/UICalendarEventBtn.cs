﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICalendarEventBtn : MonoBehaviour {

	public Text _Title;
	public Text _Content;
	public Image _Icon;
	public CalendarInfo.EventList _eventInfo;
	public Texture _Image;

	public Sprite[] _iconSprite;

	public UICalendarEventInfo _Calendarinfo;
	public void SetBtnInfo(CalendarInfo.EventList _eventInfo)
	{
		_Title.text = _eventInfo.Title;
		_Content.text = _eventInfo.Content;
		if(!string.IsNullOrEmpty(_eventInfo.ImageURL))
		{
			StartCoroutine(LoadImage(_eventInfo.ImageURL));
		}
		gameObject.GetComponent<Button>().onClick.AddListener(() => {
			if(_Calendarinfo == null)
			{
				_Calendarinfo = FindObjectOfType<UICalendarEventInfo>();
			}
			_Calendarinfo.SetInfo(_eventInfo,_Image);
		});

		_Icon.sprite = _iconSprite[_eventInfo.iconIDX-1];
	}

	IEnumerator LoadImage(string URL)
	{
		Debug.Log(URL);
		WWW w = new WWW (URL);
		yield return w;
		_Image= w.texture;
	}


}
