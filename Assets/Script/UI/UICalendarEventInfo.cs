﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICalendarEventInfo : MonoBehaviour {

	public Text _Month;
	public Text _Day;

	public Text _title;
	public RawImage _Image;
	public Text _content;

	public Button closeBtn;
	public Button LinkBtn;

	string URLLink = "";

	public GameObject _Panel;

	public string[] monthmatch;

	void Start()
	{
		closeBtn.onClick.AddListener(HidePanel);
	}

	public void SetInfo(CalendarInfo.EventList _eventInfo,Texture _image)
	{
		ShowPanel();
		_title.text =_eventInfo.Title;
		_content.text = _eventInfo.Content;
		_Month.text = monthmatch[SheetManager.Instance.selectDate.Month-1];//SheetManager.Instance.selectDate.Month.ToString ();
		_Day.text = SheetManager.Instance.selectDate.Day.ToString ();
		if(!string.IsNullOrEmpty(_eventInfo.ImageURL) && _image != null)
		{
			_Image.texture = _image;
		}

		if(!string.IsNullOrEmpty(_eventInfo.Link))
		{
			LinkBtn.gameObject.SetActive(true);
			LinkBtn.onClick.RemoveAllListeners();
			LinkBtn.onClick.AddListener(() => {Application.OpenURL(_eventInfo.Link); });
		}
		else
		{
			LinkBtn.onClick.RemoveAllListeners();
			LinkBtn.gameObject.SetActive(false);
		}
	}

	void HidePanel()
	{
		_Panel.SetActive(false);
	}

	void ShowPanel()
	{
		_Panel.SetActive(true);
	}
}
