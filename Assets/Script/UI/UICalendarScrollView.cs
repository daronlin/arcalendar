﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICalendarScrollView : MonoBehaviour {

	public GameObject _scrollViewObj;
	public ScrollRect _scrollView;
	public RectTransform _ViewPort;
	public GameObject _btnPrefab;
	public VerticalLayoutGroup _layout;

	public Button _closeBtn;

	void Start()
	{
		if(_closeBtn != null)
		{
			_closeBtn.onClick.AddListener(() => {HidePanel(); });
		}
	}

	public void SetButton(List<CalendarInfo.EventList> _eventList)
	{
		DestoryChild(_ViewPort.gameObject);
		_scrollViewObj.SetActive(true);
		_ViewPort.localPosition = new Vector3(_ViewPort.localPosition.x,0,_ViewPort.localPosition.z);

		int childcount = 0;
		for(int i = 0 ; i < _eventList.Count ;i++)
		{
			GameObject _obj = Instantiate(_btnPrefab,_ViewPort.transform);
			UICalendarEventBtn _event = _obj.GetComponent<UICalendarEventBtn>();
			_event.SetBtnInfo(_eventList[i]);
			childcount++;
		}
//		Debug.Log(_ViewPort.transform.childCount);
		_ViewPort.sizeDelta = new Vector2(_ViewPort.sizeDelta.x,120*childcount);
		_layout.childControlWidth = true;

		if(_closeBtn != null)
		{
			_closeBtn.gameObject.SetActive(true);
		}
	}

	void DestoryChild(GameObject _targetObj)
	{
		if(_targetObj.transform.childCount == 0)
		{
			return;
		}
		else
		{
			foreach(Transform child in _targetObj.transform)
			{
				Destroy(child.gameObject);
			}
		}
	}

	void HidePanel()
	{
		_scrollViewObj.SetActive(false);
		if(_closeBtn != null)
		{
			_closeBtn.gameObject.SetActive(false);
		}
	}
}
