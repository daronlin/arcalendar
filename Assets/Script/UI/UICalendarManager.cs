﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UI.Dates;

public class UICalendarManager : MonoBehaviour {

	public DatePicker InlineDatePicker;

	public bool ShowNextMonth;
	public bool ShowNextYear;
	public bool ShowWeekNumber;
	public bool ShowDatesInOtherMonths;
	public bool ShowWeekHeaders;

	void Start()
	{
		ToggleNextPreviousMonthButtons(ShowNextMonth);
		ToggleNextPreviousYearButtons(ShowNextYear);
		ToggleWeekNumberDisplay(ShowWeekNumber);
		ToggleShowDatesInOtherMonths(ShowDatesInOtherMonths);
		ToggleShowWeekHeaders(ShowWeekHeaders);
	}

	//關閉上下月按鈕
	public void ToggleShowWeekHeaders(bool on)
	{            
		InlineDatePicker.Config.WeekDays.ShowWeekHeaders = on;
		InlineDatePicker.UpdateDisplay();
	}

	//關閉上下月按鈕
	public void ToggleNextPreviousMonthButtons(bool on)
	{            
		InlineDatePicker.Config.Header.ShowNextAndPreviousMonthButtons = on;
		InlineDatePicker.UpdateDisplay();
	}

	//關閉上下年按鈕
	public void ToggleNextPreviousYearButtons(bool on)
	{
		InlineDatePicker.Config.Header.ShowNextAndPreviousYearButtons = on;
		InlineDatePicker.UpdateDisplay();
	}

	//關閉上下年按鈕
	public void ToggleWeekNumberDisplay(bool on)
	{
		InlineDatePicker.Config.WeekDays.ShowWeekNumbers = on;

		InlineDatePicker.UpdateDisplay();
	}

	//顯示其他月份
	public void ToggleShowDatesInOtherMonths(bool on)
	{
		InlineDatePicker.Config.Misc.ShowDatesInOtherMonths = on;
		InlineDatePicker.UpdateDisplay();
	}

	public void ToggleAllowMultipleDateSelection(bool on)
	{
		InlineDatePicker.DateSelectionMode = on ? DateSelectionMode.MultipleDates : DateSelectionMode.SingleDate;
		InlineDatePicker.UpdateDisplay();
	}
		
}
