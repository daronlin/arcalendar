﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UI.Dates;
public class ARCanlendarController : MonoBehaviour {

	[System.Serializable]
	public class ARCanlendarInfo
	{

		public string  ImageName;

		public enum LoadType : int
		{
			Canlendar = 0,
			prefabObject
		}

		public LoadType _loadType = LoadType.Canlendar;

		public Vector3 localPosition = Vector3.zero;
		public Vector3 localScale	= new Vector3(0.05f,0.05f,0.05f);
	}

	public List<ARCanlendarInfo> ARCamlendarInfoList = new List<ARCanlendarInfo>();
//	public Vuforia.VuforiaBehaviour _vuforiaBehaviour ;
	public GameObject _canlendarObj;
	// Use this for initialization

	public GameObject[] _ItemPrefab;

	public DatePicker _dateController;
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void ShowCanvas(GameObject _target)
	{
		bool isHavaImage = false;
		foreach(ARCanlendarInfo _selectInfo in ARCamlendarInfoList)
		{
			if(_selectInfo.ImageName == _target.name)
			{
				if(_selectInfo._loadType == ARCanlendarInfo.LoadType.Canlendar)
				{
					int month;
					month = int.Parse(_target.name.Substring(0,2));
//					Debug.LogError(month);
					isHavaImage = true;

					_canlendarObj.transform.parent = _target.transform;
					_canlendarObj.transform.localPosition = _selectInfo.localPosition;
					_canlendarObj.transform.localScale = _selectInfo.localScale;
	//				_canlendarObj.transform.localRotation = Quaternion.Euler (90.0f, 0, 0);
					foreach (RectTransform child in _target.transform) {
	//					child.gameObject.renderer.enabled = false;
						child.localRotation = Quaternion.Euler (90.0f, 0, 0);
//						Debug.Log (child.name);
					}

					_canlendarObj.SetActive(true);
					_dateController.SetDate(new System.DateTime(2018,month,1));
					break;
				}
				else
				{
					isHavaImage = true;
//					Debug.Log("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
					if (_target.transform.childCount == 0) {
//						Debug.Log (_target.name + "Child Count is 0");
						int prefabidx;
						prefabidx = int.Parse (_target.name.Substring (0, 2)) - 1;
						GameObject spownObj = Instantiate (_ItemPrefab [prefabidx], _target.transform) as GameObject;
						spownObj.transform.localPosition = _selectInfo.localPosition;
						spownObj.transform.localScale = _selectInfo.localScale;
						spownObj.transform.localEulerAngles = new Vector3 (270, 0, 180);
						break;
					} else {
						AudioScheduled _scheduled = _target.GetComponentInChildren<AudioScheduled>();
						_scheduled.enabled = true;
//						_scheduled.gameObject.SetActive (true);
//						Debug.Log(_target.name + "Child Count not 0!!!!!!!!!!!");
					}
				}
			}
		}

		if(!isHavaImage)
		{
			Debug.LogError("ARCanlendarInfo have not " + _target.name);
			_canlendarObj.SetActive(false);
			return;
		}
	}

	public void HideCanvas(GameObject _target)
	{
		foreach(ARCanlendarInfo _selectInfo in ARCamlendarInfoList)
		{
			if(_selectInfo.ImageName == _target.name)
			{
				if(_selectInfo._loadType == ARCanlendarInfo.LoadType.prefabObject)
				{
					if(_target.transform.childCount != 0)
					{
						AudioScheduled _scheduled = _target.GetComponentInChildren<AudioScheduled>();
						_scheduled.enabled = false;
						_scheduled.StopScheduledAudioAndAnimation();
						break;
					}
				}
			}
		}

		_canlendarObj.SetActive(false);
		_canlendarObj.transform.parent = null;
	}
}
