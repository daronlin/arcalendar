﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Vuforia;

public class ImageEventHandler : MonoBehaviour,ITrackableEventHandler {

	#region PRIVATE_MEMBER_VARIABLES

	private TrackableBehaviour mTrackableBehaviour;
	private ARCanlendarController _arcanlendarController;
	#endregion // PRIVATE_MEMBER_VARIABLES



	#region UNTIY_MONOBEHAVIOUR_METHODS

	void Start()
	{
		mTrackableBehaviour = GetComponent<TrackableBehaviour>();
		if (mTrackableBehaviour)
		{
			mTrackableBehaviour.RegisterTrackableEventHandler(this);
		}

		_arcanlendarController = FindObjectOfType<ARCanlendarController> ();
//		transform.localRotation = Quaternion.Euler(new Vector3(270.0f,0,0));
	}

	#endregion // UNTIY_MONOBEHAVIOUR_METHODS


	#region PUBLIC_METHODS

	/// <summary>
	/// Implementation of the ITrackableEventHandler function called when the
	/// tracking state changes.
	/// </summary>
	public void OnTrackableStateChanged(
		TrackableBehaviour.Status previousStatus,
		TrackableBehaviour.Status newStatus)
	{
		if (newStatus == TrackableBehaviour.Status.DETECTED ||
			newStatus == TrackableBehaviour.Status.TRACKED ||
			newStatus == TrackableBehaviour.Status.EXTENDED_TRACKED)
		{
			OnTrackingFound();
		}
		else
		{
			OnTrackingLost();
		}
	}

	#endregion // PUBLIC_METHODS



	#region PRIVATE_METHODS


	private void OnTrackingFound()
	{
//		Debug.Log(ARCanlendarController.Instance._canlendarObj.name);
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

//		AudioScheduled[] _audioScheduld = GetComponentsInChildren<AudioScheduled>(true);

		// Enable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = true;
		}

		// Enable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = true;
		}

		// Disable colliders:
//		foreach (AudioScheduled component in _audioScheduld)
//		{
//			component.enabled = true;
//		}

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " found");
		if (_arcanlendarController != null) {
			_arcanlendarController.ShowCanvas (gameObject);
//			ARCanlendarController.Instance.ShowCanvas(gameObject);
		}
	}


	private void OnTrackingLost()
	{
		Renderer[] rendererComponents = GetComponentsInChildren<Renderer>(true);
		Collider[] colliderComponents = GetComponentsInChildren<Collider>(true);

//		AudioScheduled[] _audioScheduld = GetComponentsInChildren<AudioScheduled>(true);

		// Disable rendering:
		foreach (Renderer component in rendererComponents)
		{
			component.enabled = false;
		}

		// Disable colliders:
		foreach (Collider component in colliderComponents)
		{
			component.enabled = false;
		}

		// Disable colliders:
//		foreach (AudioScheduled component in _audioScheduld)
//		{
////			component.enabled = false;
//			component.enabled = false;
//			component.StopScheduledAudioAndAnimation();
//		}

		Debug.Log("Trackable " + mTrackableBehaviour.TrackableName + " lost");
		if (_arcanlendarController != null) {
			_arcanlendarController.HideCanvas (gameObject);
//			ARCanlendarController.Instance.HideCanvas(gameObject);
		}
//		ARCanlendarController.Instance.HideCanvas(gameObject);
	}

	#endregion // PRIVATE_METHODS
}
