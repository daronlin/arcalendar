﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class ClassToJson : MonoBehaviour {

	public List<CalendarInfo> _originalJson = new List<CalendarInfo>();
	// Use this for initialization
	void Start () {
		
	}
	/// <summary>
	/// [{"Date":"01/01/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u5403\u98EF","ImageURL":"https://facebookbrand.com/wp-content/themes/fb-branding/prj-fb-branding/assets/images/fb-art.png","Content":"\u597D\u54E6","Link":"https://www.facebook.com/"}],"ENEventList":[]},{"Date":"01/02/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u7B2C\u4E00\u4EF6\u4E8B","ImageURL":"","Content":"\u5403\u98EF","Link":""},{"Title":"\u7B2C\u4E8C\u4EF6\u4E8B","ImageURL":"","Content":"\u7761\u89BA","Link":""}],"ENEventList":[]},{"Date":"01/03/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u7761\u89BA","ImageURL":"sleep","Content":"","Link":""},{"Title":"\u5403\u98EF","ImageURL":"eat eat","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/04/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/05/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/06/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/07/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/08/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/09/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/10/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u5403\u98EF","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/11/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/12/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u7761\u89BA","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/13/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/14/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"\u5403\u98EF","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/15/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/16/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"\u7761\u89BA","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/17/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/18/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/19/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/20/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/21/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/22/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/23/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u5403\u98EF","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/24/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/25/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"\u7761\u89BA","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/26/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/27/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/28/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/29/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"\u5403\u98EF","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/30/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"","Content":"","Link":""}],"ENEventList":[]},{"Date":"01/31/2018 00:00:00","isHaveEvent":false,"TWEventList":[{"Title":"","ImageURL":"\u7761\u89BA","Content":"","Link":""}],"ENEventList":[]}]
	/// </summary>
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.C)) {
//			;
			foreach (CalendarInfo _cache in JsonToClass (jsontxt)) 
			{
				Debug.Log (_cache.Date);
				Debug.Log (_cache.TWEventList.Count);

				string EventList = "";
				foreach (CalendarInfo.EventList i in _cache.TWEventList) {
					EventList += i.Title + "," + i.ImageURL+ ","+i.Content +i.Link+"\n";
				}
				Debug.Log(EventList);
			}
		}
	}

	public string ConverToJson(List<CalendarInfo> _inputCalendar,bool isCache = false,string SaveKEY="")
	{
		_originalJson = _inputCalendar;
		string log = "";
		log = JsonMapper.ToJson (_inputCalendar);
		Debug.Log (log);
		if (isCache) 
		{
			PlayerPrefs.SetString ("CalendarInfo"+SaveKEY, log);
		}
		return log;
	}

	public string jsontxt;
	public List<CalendarInfo> JsonToClass(string _inputJson)
	{
		List<CalendarInfo> _target = new List<CalendarInfo> ();
		_target = JsonMapper.ToObject<List<CalendarInfo>> (_inputJson);
		return _target;
	}
}
