﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google.GData.Spreadsheets;
using Google.GData.Client;
using System;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UI.Dates;

public class SheetManager : Singleton<SheetManager> {

	public const string SHEET_UPDATE_TIME	= "sheetUpdateTime";
	public const string URL_TITLE 			= "https://spreadsheets.google.com/feeds/list/{0}/{1}/public/values?alt=json";
	public const string CALENDAR_INFO		= "CalendarInfo";

	public string 	_sheetKey = ""; //Google Key 1kGchqZkECQZwMzbdCR_2Zda2eDOndTH-s0foeI_7MdM
	public int 		_sheetMinID = 1;	//Google // 預設為1
	public int		_sheetMaxID = 12;

	public Dictionary<int,List<CalendarInfo>> _DateManager = new Dictionary<int, List<CalendarInfo>>();//日期資料庫,int月份,canlendar是月的每日資訊

	public bool isUpdate = false;
	public string updateTime = "";

	private int progress = 0;
	private float Loading = 0;

	public UILoadingBar _load;

	void Awake()
	{
		DontDestroyOnLoad(gameObject);
	}

	public bool isDelAllCache = false;

	[Header("目前所選的事件資料")]
	public List<Texture> _cache = new List<Texture> ();

	// Use this for initialization
	IEnumerator Start () {

		if (isDelAllCache) {
			PlayerPrefs.DeleteAll ();
		}

		_load.OnLoadDoneEvent = LoadScene;

		//初始化表單
		for (int i = _sheetMinID; i <= _sheetMaxID; i++) 
		{
//			Debug.Log (i);
			yield return StartCoroutine (GetDataBase (i));
			progress++;
			Loading = (float)progress / (float)_sheetMaxID;
			_load.Percent = Loading;
		}

		SetCache();

//		SceneManager.LoadScene("Example Scene 1",LoadSceneMode.Single);
//		SceneManager.LoadScene("UI",LoadSceneMode.Additive);
	}
		
	void LoadScene()
	{
		Debug.Log ("Load Date Done.");
		SceneManager.LoadScene("Calendar",LoadSceneMode.Single);
		SceneManager.LoadScene("UI",LoadSceneMode.Additive);
	}

	IEnumerator GetDataBase(int idx)
	{
		//先去Request查看網路狀態
		string url = string.Format(URL_TITLE,_sheetKey,idx.ToString());
		WWW www = new WWW(url);
		yield return www;

		//如果發生網路問題,直接進入
		if (!string.IsNullOrEmpty (www.error)) 
		{
			LoadScene ();
			yield break;
//			Debug.Log("請確認網路狀態，確保能獲取完整資料。");
		}

		ListFeed newListFeed = GDocService.GetSpreadsheet(_sheetKey,idx.ToString());
		updateTime = newListFeed.Updated.ToString();

		//
		if(PlayerPrefs.HasKey(SHEET_UPDATE_TIME))
		{
			if(PlayerPrefs.GetString(SHEET_UPDATE_TIME)==updateTime)
			{
				//如果跟跟上次一樣,去載cache的資料
				isUpdate = false;

				if (PlayerPrefs.HasKey ("CalendarInfo" + idx.ToString ())) {
					Debug.Log ("CanlendarInfo " + PlayerPrefs.GetString("CalendarInfo" + idx.ToString ()));
					_DateManager[idx] = _test.JsonToClass (PlayerPrefs.GetString("CalendarInfo" + idx.ToString ()));
				}
				yield break;
			}
			else
			{
				isUpdate = true;
			}
		}
		else
		{
			isUpdate = true;
		}

		if (!_DateManager.ContainsKey (idx)) {
			_DateManager [idx] = new List<CalendarInfo> ();
		}

		foreach ( ListEntry row in newListFeed.Entries ) {
			Debug.Log( row.Title.Text );
			string showlog = "";

			CalendarInfo _cacheInfo = new CalendarInfo();

			List<CalendarInfo.EventList> cacheEvent = new List<CalendarInfo.EventList> ();

			foreach ( ListEntry.Custom element in row.Elements ) 
			{
				showlog += element.LocalName+ "   " +element.Value + " , ";

				//建立日期
				if (element.LocalName == "date") {
					string[] datesplit;
					datesplit = element.Value.Split ('/');//建立日期用/分段
					Debug.Log (element.Value);
					_cacheInfo.Date=new DateTime(int.Parse(datesplit[0]),int.Parse(datesplit[1]),int.Parse(datesplit[2]));
				}

				//解析標題
				if (element.LocalName == "title") 
				{
					if (string.IsNullOrEmpty (element.Value)) {
						cacheEvent = null;
						break;
					}
					if (cacheEvent.Count == 0) 
					{
						string[] titleList = element.Value.Split (',');
						for (int i = 0; i < titleList.Length; i++) {
							CalendarInfo.EventList _newEvent = new CalendarInfo.EventList();
							_newEvent.Title = titleList [i];
							cacheEvent.Add (_newEvent);
						}
					}
					//					cacheEvent.Title = element.Value; 
				}

				if (element.LocalName == "image") {
					string[] imageList = element.Value.Split (',');
					for (int i = 0; i < imageList.Length; i++) {
						cacheEvent[i].ImageURL = imageList [i];
					}
				}

				if (element.LocalName == "content") {
					string[] imageList = element.Value.Split (',');
					for (int i = 0; i < imageList.Length; i++) {
						cacheEvent[i].Content = imageList [i];
					}
					//					cacheEvent.Content = element.Value; 
				}

				if (element.LocalName == "link") 
				{
					//					cacheEvent.Link = element.Value;
					string[] imageList = element.Value.Split (',');
					for (int i = 0; i < imageList.Length; i++) {
						cacheEvent[i].Link = imageList [i];
					}
				}

				if (element.LocalName == "icon") 
				{
					string[] imageList = element.Value.Split (',');
					for (int i = 0; i < imageList.Length; i++) {
						if(!string.IsNullOrEmpty(imageList [i]))
						{
							cacheEvent[i].iconIDX = int.Parse(imageList [i]);
						}
					}
				}
			}

			if (cacheEvent != null) {
				_cacheInfo.TWEventList = cacheEvent;
				_DateManager [idx].Add (_cacheInfo);
			}

//			Debug.LogError( showlog );
			//			if (!string.IsNullOrEmpty (_cacheInfo.Title) && !string.IsNullOrEmpty (_cacheInfo.Content)) {
//			if (_cacheInfo.TWEventList.Count != 0 || _cacheInfo.ENEventList.Count != 0) {
//				
//			} else if(_cacheInfo.TWEventList.Count == 0 && _cacheInfo.ENEventList.Count == 0)
//			{
//				yield break;
//			}
			//			}

		}
	}

	void SetCache()
	{
		if(isUpdate)
		{
			PlayerPrefs.SetString(SHEET_UPDATE_TIME,updateTime);

			for (int i = _sheetMinID; i <= _sheetMaxID; i++) 
			{
				if(_DateManager.ContainsKey(i))
				{
//					Debug.Log("!!!!!!!!!!!_DateManager[i].Count ; " + _DateManager[i].Count);
//					_test.ConverToJson (_DateManager [_sheetMinID]);
					_test.ConverToJson (_DateManager [i], true, i.ToString());
				
//				)
				}
			}
		}
	}

	public ClassToJson _test;
	public int month;
	public int day;
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyUp (KeyCode.A)) 
		{
			if (_DateManager.ContainsKey (_sheetMinID)) 
			{
				foreach (CalendarInfo _cache in _DateManager[_sheetMinID]) 
				{
//					Debug.Log (_cache.Date);
//					Debug.Log (_cache.TWEventList.Count);

					string EventList = "";
					foreach (CalendarInfo.EventList i in _cache.TWEventList) {
						EventList += i.Title + "," + i.ImageURL+ ","+i.Content +i.Link +"\n";
					}
					Debug.Log(EventList);
				}
			}
		}

		if (Input.GetKeyUp (KeyCode.T)) {
			_test.ConverToJson (_DateManager [_sheetMinID]);
//			Debug.Log (PlayerPrefs.SetString ("CalendarInfo" + _sheetID.ToString(), log));

			if (PlayerPrefs.HasKey ("CalendarInfo" + _sheetMinID.ToString ())) {
//				Debug.Log ("CanlendarInfo " + PlayerPrefs.GetString("CalendarInfo" + _sheetMinID.ToString ()));
				_test.JsonToClass (PlayerPrefs.GetString("CalendarInfo" + _sheetMinID.ToString ()));
			}
		}
		if (Input.GetKeyUp (KeyCode.S)) {
			_test.ConverToJson (_DateManager [_sheetMinID], true, _sheetMinID.ToString());
		}

		if (Input.GetKeyUp (KeyCode.F)) {
			
			DateTime _select = new DateTime (2018, month, day);
			CalendarInfo ci = DateEvent (_select);
			if (ci != null) {
				Debug.Log (ci.Date);
				Debug.Log (ci.TWEventList.Count);
				Debug.Log (ci.ENEventList.Count);
				Debug.Log (ci.isHaveEvent);
				StartCoroutine (LoadSelectEventInfo(ci.TWEventList));
			} else {
				Debug.Log ("ci is null");
			}
		}
	}

	IEnumerator LoadSelectEventInfo(List<CalendarInfo.EventList> _select)
	{
		_cache = new List<Texture> ();
		foreach (CalendarInfo.EventList _event in _select) {
			Debug.Log("_event.ImageURL : " + _event.ImageURL);
			WWW w = new WWW (_event.ImageURL);
			yield return w;
			_cache.Add (w.texture);
		}
	}

	CalendarInfo DateEvent(DateTime _selectDate)
	{
		if (_DateManager.ContainsKey (_selectDate.Month)) {

			foreach (CalendarInfo _calendarInfo in _DateManager [_selectDate.Month]) {
				if (_calendarInfo.Date.Day == _selectDate.Day) {
					return _calendarInfo;
				}
			}
		}
		return null;
	}

	public void SetButtonEvent(DatePicker_Button DateBtn)
	{
//		Debug.Log(DateBtn.name);
		string[] dateInfo = DateBtn.name.Split('-');
		if(dateInfo.Length != 3){return;}

		DateTime selectDate = new DateTime(int.Parse(dateInfo[0]),int.Parse(dateInfo[1]),int.Parse(dateInfo[2]));

		foreach(CalendarInfo _CInfo in _DateManager[selectDate.Month])
		{
//			Debug.Log(_CInfo.Date.Month +"   "+ selectDate.Month);
			if(_CInfo.Date == selectDate)
			{
//				Debug.LogWarning(DateBtn.name + "   " + 
//					DateBtn.Button.onClick.RemoveAllListeners());
				DateBtn.Button.onClick.RemoveAllListeners();
				DateBtn.Button.onClick.AddListener(() => {OnClickEvent(DateBtn.gameObject); });
				return;
			}
		}
		DateBtn.Button.onClick.RemoveAllListeners();
	}

	public DatePickerDayButtonType SetType(DateTime _Date)
	{
//		DateTime selectDate = new DateTime(int.Parse(dateInfo[0]),int.Parse(dateInfo[1]),int.Parse(dateInfo[2]));

		foreach(CalendarInfo _CInfo in _DateManager[_Date.Month])
		{
			if(_CInfo.Date == _Date)
			{
				return DatePickerDayButtonType.SelectedDay;
			}
		}
		return DatePickerDayButtonType.CurrentMonth;
	}

	UICalendarScrollView calendarScrollView ; 
	public DateTime selectDate;
	void OnClickEvent(GameObject _target)
	{
		string[] dateInfo = _target.name.Split('-');
		if(dateInfo.Length != 3){return;}

		selectDate = new DateTime(int.Parse(dateInfo[0]),int.Parse(dateInfo[1]),int.Parse(dateInfo[2]));

		foreach(CalendarInfo _CInfo in _DateManager[selectDate.Month])
		{
			if(_CInfo.Date == selectDate)
			{
				calendarScrollView = FindObjectOfType<UICalendarScrollView>();
				calendarScrollView.SetButton(_CInfo.TWEventList);
				return;
			}
		}


//		UICalendarScrollView.Instance.SetButton();
	}
}
	
[System.Serializable]
public class CalendarInfo
{
	public DateTime Date;
	public bool isHaveEvent=false;
	public List<EventList> TWEventList = new List<EventList>();
	public List<EventList> ENEventList = new List<EventList>();

	public class EventList
	{
		public string Title ="";
		public string ImageURL ="";
		public string Content ="";
		public string Link = "";
		public int iconIDX = 1;
	}
}
