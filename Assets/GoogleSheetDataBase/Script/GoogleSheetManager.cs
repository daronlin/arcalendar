﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Google.GData.Spreadsheets;
using Google.GData.Client;

public class GoogleSheetManager : MonoBehaviour {

	public string URLTitle = "https://spreadsheets.google.com/feeds/list/{0}/{1}/public/values?alt=json";
	public string Key	   = "1xEMtSfH1O1QJao_eXAAAubPnfT8kltJHWdBoka_yfkA";
	public string sheet_index = "1"; //預設是1

	public string url;// = "http://images.earthcam.com/ec_metros/ourcams/fridays.jpg";
	IEnumerator Start()
	{
		url = string.Format (URLTitle, Key, sheet_index);
		WWW www = new WWW(url);
		yield return www;
//		Debug.Log (www.text);
		ListFeed newListFeed = GDocService.GetSpreadsheet (Key,sheet_index);
//		Debug.Log (newListFeed.Categories.Count);
//		Debug.Log (newListFeed.Entries.Count);//現在該表單有幾筆資料
//		Debug.Log (newListFeed.Authors[0].Name);//作者名稱
		Debug.Log (newListFeed.Updated.ToString());//最後更新時間
//		Debug.Log (newListFeed.Title.Text);
//		Debug.Log (newListFeed.Entries);


		foreach ( ListEntry row in newListFeed.Entries ) {
			// Print the first column's cell value
			Debug.Log( row.Title.Text );
			// Iterate over the remaining columns, and print each cell value
			string showlog = "";
			foreach ( ListEntry.Custom element in row.Elements ) {
				showlog += element.Value + " , ";
			}
			Debug.LogError( showlog );
		}


		//存成xml
		System.IO.StreamWriter target = new System.IO.StreamWriter (Application.dataPath+"555.xml");
		newListFeed.SaveToXml (target.BaseStream);



//		Debug.Log (Application.dataPath + "555.xml");

//		Debug.Log (newListFeed.SaveToXml();
//		foreach (AtomEntryCollection me in newListFeed.Entries) 
//		{
//			Debug.Log (me [0].XmlName);
//		}

//		Debug.Log (newListFeed.Entries[1].ExtensionElements[1].XmlName);
//		Debug.Log (newListFeed.Entries[1].ExtensionElements[1].XmlNameSpace);
//		Debug.Log (newListFeed.Entries[1].ExtensionElements[1].XmlPrefix);
//		newListFeed.Entries [0].Title
//		Debug.Log (newListFeed.Authors[0].XmlName);//作者名稱
		Debug.Log (www.text);
//		Renderer renderer = GetComponent<Renderer>();
//		renderer.material.mainTexture = www.texture;
	}

	void Awake()
	{
		InvokeRepeating("RepeatStart",1,5);
	}

	void RepeatStart()
	{
		StartCoroutine (Start ());
	}
}
